from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm
from .models import Project


# Create your views here.
@login_required
def list_projects(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {"project_list": project_list}
    return render(request, "projects/list_projects.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {"project": project}
    return render(request, "projects/show_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.instance.owner = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
